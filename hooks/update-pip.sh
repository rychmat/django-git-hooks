#!/bin/bash
#
# Hook checking that requirements.txt is identical that pip freeze and updating requirements.txt
#
################################################################################################

#########################################
# Compare freeze and requirements.txt ###
#########################################

work_dir="$(git rev-parse --show-toplevel)"
source_dir="$(dirname "$0")"

source $source_dir/config
source $source_dir/msg.sh

if source "$VIRTUAL_ENV/bin/activate"
then
	touch "$work_dir/requirements.txt"

	if grep -vxFf <(cat "$work_dir/requirements.txt" "$work_dir/.pipignore" 2> /dev/null) <(pip freeze) > /dev/null
	then
		echo -n "Updating requirements "
		updated_packages="$(\grep -vxFf <(cat "$work_dir/requirements.txt" "$work_dir/.pipignore" 2> /dev/null) <(pip freeze) | tr '\n' ' ')"
		if [ -f "$work_dir/.pipignore" ]
		then
			pip freeze | \grep -v $(cat "$work_dir/.pipignore") > "$work_dir/requirements.txt"
		else
			pip freeze > "$work_dir/requirements.txt"
		fi
		git add "$work_dir/requirements.txt"
		echo_success "success (Updated packages: $updated_packages)"
	else
		echo_info "File requirements.txt need not be updated"
	fi
else
	echo_error "Cannot found virtualenv"
fi
