#!/bin/sh
#
# Hook checking that bower.json or package.json is identical to previous revision
#
#################################################################################

##################################################################
# Compare bower.json and package.json to previous version      ###
##################################################################

source_dir="$(dirname "$0")"

source $source_dir/msg.sh

bower_diff=`git diff @{-1}:bower.json bower.json`

if [ ! -z "$bower_diff" ]
then
    echo_info "Installing Bower dependencies..."
    bower install
    echo_success
fi

npm_diff=`git diff @{-1}:package.json package.json`

if [ ! -z "$npm_diff" ]
then
    echo_info "Installing npm dependencies..."
    npm install
    echo_success
fi
