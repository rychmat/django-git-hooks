#!/bin/bash
#
# Hook checking that requirements.txt is identical that pip freeze and updating packages from requirements.txt
#
##############################################################################################################

##################################################################
# Compare freeze and requirements.txt and install new packages ###
##################################################################

work_dir="$(git rev-parse --show-toplevel)"
source_dir="$(dirname "$0")"

source $source_dir/config
source $source_dir/msg.sh

echo "Purging pyc files and empty directories..."

# Start from the repository root.
cd ./$(git rev-parse --show-cdup)

# Delete .pyc files and empty directories.
find . -name "*.pyc" -delete 2>&1 > /dev/null
find . -type d -empty -delete 2>&1 > /dev/null

if source "$VIRTUAL_ENV/bin/activate"
then

	echo -n "Checking if virtualenv needs to update packages "
	touch "$work_dir/requirements.txt"

	if grep -vxFf <(pip freeze) "$work_dir/requirements.txt"
	then
		echo "\nInstalling packages "
		make pip
		echo_success
	else
		echo_info "Packages need not to be updated"
	fi

	./$source_dir/migrate-db-and-init-data.sh
else
	echo_error "Cannot found virtualenv"
fi

./$source_dir/bower-npm-hook.sh

./$source_dir/build-client.sh

